from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def home(request):
    return render(request, 'menyapa/home.html')

def masuk(request):
    if (request.method == 'POST'):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if (user is not None):
            login(request, user)
            return redirect('/menyapa')
        else:
            return redirect('/menyapa/gagal')
    else:
        return render(request, 'menyapa/login.html')

def gagal(request):
    return render(request, 'menyapa/gagal.html')

def daftar(request):
    if (request.method == 'POST'):
        username = request.POST['username']
        password = request.POST['password']
        user = User.objects.create_user(username, '', password)
        login(request, user)
        return redirect('/menyapa')
    else:
        return render(request, 'menyapa/register.html')

def keluar(request):
    logout(request)
    return redirect('/menyapa')