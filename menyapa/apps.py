from django.apps import AppConfig


class MenyapaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'menyapa'
