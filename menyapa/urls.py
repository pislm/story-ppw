from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='menyapa'),
    path('masuk', views.masuk, name='masuk'),
    path('keluar', views.keluar, name='keluar'),
    path('daftar', views.daftar, name='daftar'),
    path('gagal', views.gagal, name='gagal'),
]
