from django.urls import path

from . import views

app_name = 'catatmatkul'

urlpatterns = [
    path('', views.home, name='catatmatkul'),
    path('lihat', views.lihatmatkul, name="lihatmatkul"),
    path('lihat/<int:id>', views.detailmatkul, name="detailmatkul"),
    path('tambah', views.tambahmatkul, name="tambahmatkul"),
    path('hapus', views.hapusmatkul, name="hapusmatkul"),
    path('hapus/<int:id>', views.konfirmasihapus, name="konfirmasihapus"),
]