from django.apps import AppConfig


class CatatmatkulConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'catatmatkul'
