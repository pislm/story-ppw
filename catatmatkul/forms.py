from django.forms import ModelForm
from .models import Matkul

class MatkulForm(ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'