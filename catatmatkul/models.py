from django.db import models

class Matkul(models.Model):
    nama = models.CharField(max_length=30)
    dosen_pengajar = models.CharField(max_length=30)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    semester_tahun = models.CharField(max_length=15)
    ruang_kelas = models.CharField(max_length=10)

    def __str__(self) -> str:
        return self.nama