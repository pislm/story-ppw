from django.http import response
from django.shortcuts import redirect, render
from .models import Matkul
from catatmatkul.forms import MatkulForm

def home(request):
    return render(request, 'catatmatkul/index.html')

def lihatmatkul(request):
    matkuls = Matkul.objects.values()
    response = {}
    response['matkuls'] = matkuls
    return render(request, 'catatmatkul/lihatmatkul.html', response)

def detailmatkul(request, id):
    matkul = Matkul.objects.values().get(pk=id)
    return render(request, 'catatmatkul/detailmatkul.html', matkul)

def tambahmatkul(request):
    form = MatkulForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form = MatkulForm(request.POST)
        form.save()
        return redirect('/catatmatkul/lihat')
    else:
        return render(request, 'catatmatkul/tambahmatkul.html',{'form':form})

def hapusmatkul(request):
    matkuls = Matkul.objects.values()
    response = {}
    response['matkuls'] = matkuls
    return render(request, 'catatmatkul/hapusmatkul.html', response)

def konfirmasihapus(request, id):
    if request.method == "POST":
        matkul = Matkul.objects.get(pk=id)
        matkul.delete()
        return redirect('/catatmatkul/hapus')
    matkul = Matkul.objects.values().get(pk=id)
    return render(request, 'catatmatkul/konfirmasihapus.html', matkul)