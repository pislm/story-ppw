from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def porto(request):
    return render(request, 'main/porto.html')

def story1(request):
    return render(request, 'main/story1.html')