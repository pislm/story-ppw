from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('porto', views.porto, name='porto'),
    path('story1', views.story1, name='story1'),
]