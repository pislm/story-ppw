from catatkegiatan.models import Kegiatan, Peserta
from django.contrib import admin

# Register your models here.
admin.site.register(Kegiatan)
admin.site.register(Peserta)