from catatkegiatan.forms import KegiatanForm, PesertaForm
from catatkegiatan.models import Kegiatan, Peserta
from django.shortcuts import render, redirect

# Create your views here.
def home(request):
    daftar_kegiatan = Kegiatan.objects.all().values()
    dict_kegiatan = {}
    for kegiatan in daftar_kegiatan:
        daftar_peserta = Peserta.objects.filter(kegiatan = kegiatan['id']).values()
        dict_kegiatan[kegiatan['id']] = {
            'id' : kegiatan['id'],
            'nama' : kegiatan['nama'],
            'peserta' : [peserta for peserta in daftar_peserta]
        }
    return render(request, 'catatkegiatan/index.html', {'dict_kegiatan' : dict_kegiatan.items()})

def tambahkegiatan(request):
    form = KegiatanForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/catatkegiatan')
    else:
        return render(request, 'catatkegiatan/tambahkegiatan.html', {'form' : form})

def tambahpeserta(request, id):
    form = PesertaForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/catatkegiatan')
    else:
        context = {
            'form' : form,
            'id' : id,
            'namakegiatan' : Kegiatan.objects.values().get(pk=id)['nama'],
        }
        return render(request, 'catatkegiatan/tambahpeserta.html', context)