from django import forms
from django.forms import fields
from .models import Kegiatan, Peserta

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'