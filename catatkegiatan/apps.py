from django.apps import AppConfig


class CatatkegiatanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'catatkegiatan'
