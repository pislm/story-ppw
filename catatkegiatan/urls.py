from django.urls import path
from . import views

app_name = "catatkegiatan"

urlpatterns = [
    path('', views.home, name='catatkegiatan'),
    path('tambahkegiatan', views.tambahkegiatan, name='tambahkegiatan'),
    path('tambahpeserta/<int:id>', views.tambahpeserta, name='tambahpeserta'),
]
